## Cloning

This project uses the concept of [git submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules). To properly clone the project, you need to run this command:
```
git clone --recurse-submodules https://github.com/kuru-project/tamaki-js.git
```
